#include <EEPROM.h>
#include <AccelStepper.h>
AccelStepper motor (1,3,2); 

#define SIZE 200
int pressureWindow[SIZE];
int pressureChar[2];  // pressure characterstics 

// SESSION DESCRIPTION:
int sessionTime=30;
int injectionRate=5;
int constant=162;



// Sensors Pin configurations:
int alarmGPin = 11;
int alarmTPin = 12;
int alarmPPin = 13;
int machineStop = 14;
int glucosePin = A0;
int pressurePin = A1;
int temperaturePin = A2;
int initGlucose;

// Pateint's Data:
struct PatientData{
  int   ID;
  char Name[25];
  int  Weight;
  int   Age;
  char Gender;
};
  PatientData patient;

// pressure characterstics
int currentSystole;
int currentDiastole;

int currentGlucose;
int mappedGlucode;
int currentTemp;
int mappedTemp;
int syringIndex=0;

// Thresholds:
int tempThreshold=39;
int glucoseThreshold=60;
int systoleThreshold=80;
int diastoleThreshold=40;

void setup() {
  
  Serial.begin(9600);
  pinMode(alarmGPin,OUTPUT);
  pinMode(alarmTPin,OUTPUT);
  pinMode(alarmPPin,OUTPUT);
  pinMode(machineStop,OUTPUT);
  digitalWrite(alarmGPin,LOW);
  digitalWrite(alarmTPin,LOW);
  digitalWrite(alarmPPin,LOW);
// initial prints of the patient data;
  savePatientInfo();
// Check the patient Glucose level for the first time once.
  currentGlucose = analogRead(glucosePin);
  initGlucose = map(currentGlucose,614,1024,40,150);  // 614 corresponding to 3V
  
  
  // configure stepper motor
    motor.setMaxSpeed(10000);
    motor.setAcceleration(800);      

  
}

void loop() {
// Sensor Readings:
  checkGlucose();
  checkTemp();
  checkPressure();
  pumpControl();
// Patient Status:
  patientStatus();
}

void pumpControl(){ // Control the Stepper Motor
  if(currentGlucose>=initGlucose-(patient.Weight/constant)*injectionRate*sessionTime){
    syringIndex += injectionRate ; 
    motor.moveTo(syringIndex); 
    motor.run();  
  }
  else{
    digitalWrite(machineStop,HIGH);
    exit(0);
  }
    /*

   */
}

void patientStatus(){
  Serial.print("Glucose Level : ");
  Serial.print(currentGlucose);
  Serial.print("      Temperature : ");
  Serial.print(currentTemp);
  Serial.print("      Pressure : ");
  Serial.print(currentSystole);
  Serial.print("/");
  Serial.println(currentSystole);
}

void savePatientInfo(){
  int i=0;

  Serial.println("Insert patient id:");
  patient.ID = Serial.parseInt();
  
  Serial.println("Insert the patient name:");
  if(Serial.available()){
     delay(100);
     while( Serial.available() && i< 24 && patient.Name[i]!='\n') {
        patient.Name[i++] = Serial.read();
     }
     patient.Name[i++]='\0';
  }
  if(i>0)
     Serial.println((char*)patient.Name);

  Serial.println("Insert patient age:");
  patient.Age = Serial.parseInt();

  Serial.println("Insert patient Gender:");
  patient.Gender = Serial.read();

  Serial.println("Insert patient Weight:");
  patient.Weight = Serial.parseInt();
  
  
  //EEPROM.put(0, patient );
  
  Serial.print("Name: ");
  Serial.print(patient.Name);
  Serial.print("\tID: ");
  Serial.print(patient.ID);
  Serial.print("\tWeight: ");
  Serial.print(patient.Gender);
  Serial.print("\tAge: ");
  Serial.print(patient.Age);
  Serial.print("\tGender: ");
  Serial.println(patient.Gender);
}


void checkTemp(){
  int currentTemp = analogRead(temperaturePin);
  int mappedTemp=map(currentTemp,512,922,25,45);  // 512 -->> 2.5V & 922 -->> 4.5V
  if(mappedTemp>=tempThreshold){
    digitalWrite(alarmTPin,HIGH);
  }
}
void checkGlucose(){  // scale 0 : 1024 mapped to 0 : 150
  int currentGlucose = analogRead(glucosePin);
  int mappedGlucode = map(currentGlucose,614,1024,40,150);  // 614 corresponding to 3V
  if(mappedGlucode<=glucoseThreshold){
    digitalWrite(alarmGPin,HIGH);
  }
}
void checkPressure(){
//Record the pressure for two seconds considering that each sample takes 1 millisecond
  int i;
  for(i=0;i<200;i++){
    int currentPressure = analogRead(pressurePin);
    int mappedPressure=map(currentPressure,0,1024,0,255);
    pressureWindow[i]=mappedPressure;
    delay(25);
  }
  pressureTriangulation(pressureWindow);
  if(currentSystole<=systoleThreshold || currentDiastole<=diastoleThreshold){
    digitalWrite(alarmPPin,HIGH);
  }
}
void pressureTriangulation(int* pressureWindow){  // process the array of pressure samples to get the current systolic & diastolic
  int currentSystole=getMaxValue(pressureWindow);
  int currentDiastole=getMinValue(pressureWindow);
  pressureChar[0]=currentSystole;
  pressureChar[1]=currentDiastole;
}

int getMaxValue(int* array)
  {
    
   int mxm = array[0];
   int i;
     for (i=0; i<SIZE; i++) {
        if (array[i]>mxm) {
          mxm = array[i];
        }
     }
  return mxm; 
   
  }
int getMinValue(int* array)
  {
   
  int mn = array[0];
  int i;
   for (i=0; i<SIZE; i++) {
     if (array[i]<mn) {
        mn = array[i];
    }
  }
  return mn;
   
  }


